import { SafeAppProvider } from '@gnosis.pm/safe-apps-provider';
import { SafeInfo } from '@gnosis.pm/safe-apps-sdk';
import SafeAppsSDK from '@gnosis.pm/safe-apps-sdk/dist/src/sdk';
import { Contract, ethers } from 'ethers';
import { useState, useEffect, useDebugValue, useMemo } from 'react';

export declare type Info = {
    address: string,
    name: string,
    symbol: string,
    owner: string,
    contract: Contract
}

export default function useSpshe(sdk: SafeAppsSDK, safe: SafeInfo): [Info, boolean] {
    const web3Provider = useMemo(() => new ethers.providers.Web3Provider(new SafeAppProvider(safe, sdk)), [sdk, safe]);
    const erc20ABI = [
        "function name() view returns (string)",
        "function symbol() view returns (string)",
        "function balanceOf(address) view returns (uint)",
        "function transfer(address to, uint amount)",
        "function pause()",
        "event Transfer(address indexed from, address indexed to, uint amount)",
        "function owner() view returns (address)"
    ];

    const contract = new ethers.Contract("0xC3eC0B51c52edc3A70f98A089aF343dff573C54f", erc20ABI, web3Provider);
    const [info, setInfo] = useState<Info>({
        address: "loading",
        name: "loading",
        symbol: "loading",
        owner: "loading",
        contract: contract
    });
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        async function fetch() {
            const name = await contract.name()
            const symbol = await contract.symbol()
            setInfo({
                address: contract.address,
                name: name,
                symbol: symbol,
                owner: await contract.owner(),
                contract: contract
            })
            setLoaded(true);
        }

        fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [sdk]);

    useDebugValue("Spshe" ?? "loading...")

    return [info, loaded];
}
