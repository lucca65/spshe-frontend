import React, { useCallback } from 'react'
import styled from 'styled-components'
import { Button, Title } from '@gnosis.pm/safe-react-components'
import { useSafeAppsSDK } from '@gnosis.pm/safe-apps-react-sdk'
import useSpshe from './hooks/useSpshe';

const Container = styled.div`
  padding: 1rem;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const Link = styled.a`
  margin-top: 8px;
`

const SafeApp = (): React.ReactElement => {
  const { sdk, safe } = useSafeAppsSDK()
  const [{ symbol, address, contract, owner }] = useSpshe(sdk, safe)

  const _submitTx = useCallback(async () => {
    sdk.txs.send({
      txs: [
        {
          to: address,
          value: '0',
          data: contract.interface.encodeFunctionData("pause"),
        },
      ],
    }).then(x => {
      console.log('foi mlk', x)
    })
      .catch(console.error)

  }, [sdk, address, contract])


  // const _submitTx = useCallback(async () => {
  //   try {
  //     console.log('here!!')
  //     const { safeTxHash } = await sdk.txs.send({
  //       txs: [
  //         {
  //           to: address,
  //           value: '0',
  //           data: contract.interface.encodeFunctionData("pause"),
  //         },
  //       ],
  //     })
  //     console.log({ safeTxHash })
  //     const safeTx = await sdk.txs.getBySafeTxHash(safeTxHash)

  //     console.log({ safeTx })
  //   } catch (e) {
  //     console.error(e)
  //   }
  // }, [sdk, address, contract])

  return (
    <Container>
      <Title size="md">Safe: {safe.safeAddress}</Title>
      <Title size="sm">Contract Address: <small>{address}</small></Title>
      <Title size="sm">Contract Symbol: <small>{symbol}</small></Title>
      <Title size="sm">Is Safe owner: {safe.safeAddress === owner ? '✅' : '🚫'}</Title>


      <Button size="lg" color="primary" onClick={_submitTx}>
        Click to pause the contract
      </Button>

      <Link href="https://www.craft.do/s/YPgIN40ABFkE4E" target="_blank" rel="noreferrer">
        Documentation
      </Link>
    </Container>
  )
}

export default SafeApp
